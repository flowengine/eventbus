# FlowEngine EventBus #

FlowEngine EventBus is part of the FlowEngine project. This module intends to be an integration of greenrobot/EventBus to FlowEngine. EventBus is a key component in mobile apps because it helps ease the communication between different processes and helps us avoid race conditions between background running tasks and UI threads. It also helps us integrate components in a loosely-coupled way.

See [https://packagecloud.io/flowengine/injector](Link URL) for installation steps