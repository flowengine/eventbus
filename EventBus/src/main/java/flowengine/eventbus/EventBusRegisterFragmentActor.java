package flowengine.eventbus;

import android.app.Activity;
import android.support.v4.app.Fragment;
import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 04/09/2016.
 */
public class EventBusRegisterFragmentActor implements Actor<Fragment> {

    private EventBus bus;

    public void execute(Fragment fragment) {
        bus.register(fragment);
    }
}
