package flowengine.eventbus;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 09/06/2015.
 */
public interface EventBus {

    boolean isRegistered(Object subscriber);

    void cancelEventDelivery(Object event);

    <T> T getStickyEvent(Class<T> eventType);

    <T> T removeStickyEvent(Class<T> eventType);

    boolean removeStickyEvent(Object event);

    void removeAllStickyEvents();

    boolean hasSubscriberForEvent(Class<?> eventClass);

    void postSticky(Object listener);

    void register(Object listener);

    void unregister(Object listener);

    void post(Object event);
}
