package flowengine.eventbus;

import flowengine.injection.Configurator;
import flowengine.injection.IoCContainer;
import org.greenrobot.eventbus.EventBusBuilder;

/**
 * Created by Ignacio on 04/09/2016.
 */
public class EventBusConfigurator implements Configurator {

    private EventBusBuilder builder;

    public EventBusConfigurator(EventBusBuilder builder) {
        builder = builder;
    }

    public EventBusConfigurator() {
        builder = null;
    }

    public void configure(IoCContainer ioCContainer) {
        ioCContainer.registerSingleton(EventBus.class, new EventBusAdapter(BuildEventBus()));
    }

    protected org.greenrobot.eventbus.EventBus BuildEventBus() {
        if (builder != null)
            builder.installDefaultEventBus();
        return org.greenrobot.eventbus.EventBus.getDefault();
    }
}
