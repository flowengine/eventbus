package flowengine.eventbus;

import android.app.Activity;
import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 04/09/2016.
 */
public class EventBusUnregisterActivityActor implements Actor<Activity> {

    private EventBus bus;

    public void execute(Activity activity) {
        bus.unregister(activity);
    }
}
