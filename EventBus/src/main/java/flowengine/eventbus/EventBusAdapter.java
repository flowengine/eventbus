package flowengine.eventbus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 09/06/2015.
 */
public class EventBusAdapter {

    private EventBus innerBus;

    EventBusAdapter(EventBus bus) {
        innerBus = bus;
    }

    public boolean isRegistered(Object subscriber) {
        return innerBus.isRegistered(subscriber);
    }

    public void cancelEventDelivery(Object event) {
        innerBus.cancelEventDelivery(event);
    }

    public <T> T getStickyEvent(Class<T> eventType) {
        return innerBus.getStickyEvent(eventType);
    }

    public <T> T removeStickyEvent(Class<T> eventType) {
        return innerBus.removeStickyEvent(eventType);
    }

    public boolean removeStickyEvent(Object event) {
        return innerBus.removeStickyEvent(event);
    }

    public void removeAllStickyEvents() {
        innerBus.removeAllStickyEvents();
    }

    public boolean hasSubscriberForEvent(Class<?> eventClass) {
        return innerBus.hasSubscriberForEvent(eventClass);
    }

    public void postSticky(Object listener) {
        innerBus.postSticky(listener);
    }

    public void register(Object listener) {
        if (listener != null && HasAnnotatedMethodSubscriber(listener))
            innerBus.register(listener);
    }

    public void unregister(Object listener) {
        innerBus.unregister(listener);
    }

    public void post(Object event) {
        innerBus.post(event);
    }

    private boolean HasAnnotatedMethodSubscriber(Object listener) {
        for (Method method : listener.getClass().getMethods()) {
            if (method.getAnnotation(Subscribe.class) != null){
                return true;
            }
        }
        return false;
    }
}
