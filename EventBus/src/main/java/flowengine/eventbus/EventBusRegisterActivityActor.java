package flowengine.eventbus;

import android.app.Activity;
import flowengine.orchestra.orchestrator.Actor;
import org.greenrobot.eventbus.*;

/**
 * Created by Ignacio on 04/09/2016.
 */
public class EventBusRegisterActivityActor implements Actor<Activity> {

    private EventBus bus;

    public void execute(Activity activity) {
        bus.register(activity);
    }
}
